import Head from "next/head";
import data from "../mockData/data.json";
import ProductList from "../components/product-list/product-list";

// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }

export const getStaticProps = async () => {
  return {
    props: {
      data: data,
    },
  };
};

const Home = ({ data }) => {
  let items = data.products;
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1 className="navbar">Dishwashers ({items.length})</h1>
        <ProductList products={items} />
      </div>
    </div>
  );
};

export default Home;
