import ProductCarousel from "../../components/product-carousel/product-carousel";
import products from "../../mockData/data2.json";
import styles from "./product-detail.module.scss";
import Link from "next/link";

// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();

//   return {
//     props: { data },
//   };
// }

export async function getStaticPaths() {
  const paths = products.detailsData.map((product) => ({
    params: { id: product.productId.toString() },
  }));

  return { paths, fallback: false };
}

export const getStaticProps = async ({ params }) => {
  const data = products.detailsData.find(
    (product) => product.productId === params.id
  );
  return {
    props: {
      data: data,
    },
  };
};

const ProductDetail = ({ data }) => {
  return (
    <div>
      <h1 className="navbar">
        <Link
          key={data.productId}
          href={{
            pathname: "/",
          }}
        >
          <a className={styles.backBtn}>🡠</a>
        </Link>
        {data.title}
      </h1>
      <div className={styles.productPage}>
        <ProductCarousel
          image={data.media.images.urls[0]}
          imgDesc={data.title}
        />
        <div className={styles.content}>
          <div className={styles.pricing}>
            <h1 className={styles.pricing__price}>£{data.price.now}</h1>
            <h3 className={styles.pricing__specialOffer}>
              {data.displaySpecialOffer}
            </h3>
            <h3 className={styles.pricing__includedServices}>
              {data.additionalServices.includedServices}
            </h3>
          </div>
          <div className={styles.productInformation}>
            <h3 className={styles.productInformation__title}>
              Product information
            </h3>
            <p className={styles.productInformation__productCode}>
              Product Code: {data.code}
            </p>
            <p
              className={styles.productInformation__description}
              dangerouslySetInnerHTML={{
                __html: data.details.productInformation,
              }}
            ></p>
          </div>
          <h3 className={styles.productSpec}>Product specification</h3>
          <ul className={styles.productSpecs}>
            {data.details.features[0].attributes.map((item) => (
              <li key={item.id} className={styles.productSpecs__spec}>
                <p>{item.name}</p>
                <p>{item.value}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
