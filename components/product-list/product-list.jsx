import Link from "next/link";
import styles from "./product-list.module.scss";
import ProductListItem from "../product-list-item/product-list-item";
import { useState } from "react";

const ProductList = ({ products }) => {
  const [page, setPage] = useState(0);

  const getPageButtons = () => {
    const pageButtons = [];
    for (let i = 0; i < Math.ceil(products / 20); i++) {
      pageButtons.push(<button onClick={() => setPage(i + 1)}>{i + 1}</button>);
    }
    return pageButtons;
  };

  return (
    <div>
      <div className={styles.content}>
        {
          // use slice here to only display the first 20
          // but still display all the possible results at the top of the page
          products.slice(page * 20, page * 20 + 20).map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <ProductListItem
                  image={item.image}
                  description={item.title}
                  price={item.price.now}
                />
              </a>
            </Link>
          ))
        }
      </div>

      <div className={styles.pageButtons}>
        {Array.from({ length: Math.ceil(products.length / 20) }, (_, i) => (
          <button
            className={styles.pageButton}
            key={i}
            onClick={() => setPage(i)}
          >
            {i + 1}
          </button>
        ))}
      </div>
    </div>
  );
};

export default ProductList;
