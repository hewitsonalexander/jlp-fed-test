import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description }) => {
  return (
    <div className={styles.content}>
      <div>
        <img src={image} alt={description} style={{ width: "100%" }} />
      </div>
      <div className={styles.description}>{description}</div>
      <div className={styles.price}>{price}</div>
    </div>
  );
};

export default ProductListItem;
