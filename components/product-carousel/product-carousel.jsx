import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ image, imgDesc }) => {
  return (
    <div className={styles.productCarousel}>
      <img
        src={image}
        alt={imgDesc}
        style={{ width: "100%", maxWidth: "500px" }}
      />
    </div>
  );
};

export default ProductCarousel;
